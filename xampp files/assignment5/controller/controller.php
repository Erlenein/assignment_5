<?php
	require_once("model/Model.php");
	class Controller{
		protected $model = null;
		public function __construct() {
			$this->model = new Model();
		}
		public function invoke() {
			$this->model->master();
		}
	}
?>