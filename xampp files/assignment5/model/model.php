<?php
	class Model{
		public $dom;
		public $db;
		public $xpath;
		public $userNames;
		public $clubIDs;
		public $distance_15 = array();
		
		public function __construct(){
			$this->db = new PDO('mysql:host=localhost; dbname=assignment_5; charset=utf8', 'root', '');
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$this->dom = new DOMDocument();
			$this->dom->load('Model/SkierLogs.xml');
			$this->xpath = new DOMXPath($this->dom);
		}
		
		public function getSeason() {
			$arr=array();
			$potato=$this->dom->getElementsByTagName("Season");
			foreach($potato as $p) {
				array_push($arr, $p->getAttribute("fallYear"));
			}
			for($i = 0; $i < sizeOf($arr); $i++) {
				try {
					$input=$this->db->prepare("INSERT INTO season(fallYear) VALUES (:fallYear)");
					$input->bindValue(':fallYear', $arr[$i], PDO::PARAM_STR);
					$input->execute();
					echo "Season\o/ ";
				}
				catch(Exception $e){
					echo "Something went wrong - Season - ";
					echo $e->getMessage();
				}
			}
		}
		
		public function getClubs() {
			$arr1=array();
			$arr2=array();
			$arr3=array();
			$arr4=array();
			$id=    $this->dom->getElementsByTagName("Club");
			$name=  $this->dom->getElementsByTagName("Name");
			$city=  $this->dom->getElementsByTagName("City");
			$county=$this->dom->getElementsByTagName("County");
			foreach($id as $i) {
				array_push($arr1, $i->getAttribute("id"));
			}
			foreach($name as $n) {
				array_push($arr2, $n->nodeValue);
			}
			foreach($city as $ci) {
				array_push($arr3, $ci->nodeValue);
			}
			foreach($county as $co) {
				array_push($arr4, $co->nodeValue);
			}

			for($x = 0; $x < sizeOf($arr1); $x++) {
				try{
					$input=$this->db->prepare("INSERT INTO clubs(id, clubName, city, county) VALUES (:id, :name, :city, :county)");
					$input->bindValue(':id', 	 $arr1[$x], PDO::PARAM_STR);
					$input->bindValue(':name', 	 $arr2[$x], PDO::PARAM_STR);
					$input->bindValue(':city', 	 $arr3[$x], PDO::PARAM_STR);
					$input->bindValue(':county', $arr4[$x], PDO::PARAM_STR);
					$input->execute();
					echo"Club\o/ ";
				}
				catch(Exception $e) {
					echo "Something went wrong - Clubs - ";
					echo $e->getMessage();
				}
			}
		}
		
		public function getSkiers() {
			$arr1=array();
			$arr2=array();
			$arr3=array();
			$arr4=array();
			$userName  = $this->dom->getElementsByTagName("Skier");
			$FirstName = $this->dom->getElementsByTagName("FirstName");
			$LastName  = $this->dom->getElementsByTagName("LastName");
			$YOB       = $this->dom->getElementsByTagName("YearOfBirth");
			foreach($userName as $u) {
				array_push($arr1, $u->getAttribute("userName"));
			}
			foreach($FirstName as $f) {
				array_push($arr2, $f->nodeValue);
			}
			foreach($LastName as $l) {
				array_push($arr3, $l->nodeValue);
			}
			foreach($YOB as $y) {
				array_push($arr4, $y->nodeValue);
			}
			$arr1 = array_unique($arr1);
			for($x = 0; $x < sizeOf($arr1); $x++) {
				try {
					$input=$this->db->prepare("INSERT INTO skier(userName, firstName, lastName, yearOfBirth) VALUES (:userName, :firstName, :lastName, :yearOfBirth)");
					$input->bindValue(':userName',    $arr1[$x], PDO::PARAM_STR);
					$input->bindValue(':firstName',   $arr2[$x], PDO::PARAM_STR);
					$input->bindValue(':lastName', 	  $arr3[$x], PDO::PARAM_STR);
					$input->bindValue(':yearOfBirth', $arr4[$x], PDO::PARAM_STR);
					$input->execute();
					//echo"Skiers\o/ ";
				}
				catch(Exception $e) {
					echo "Something went wrong - Skiers - ";
					echo $e->getMessage();
				}
			}
		}
		
		public function getSCS() {
				$sum = 0;
                $participatingUserNames = array();
				$userClubIds = array();
				$participatingDistance = array();
				//$temp = array();
					//Getting usernames for season 2015.
				$temp = $this->xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier/@userName');
                foreach($temp as $t){
                    array_push($participatingUserNames, trim($t->nodeValue));
                }
					//Getting users clubs for 2015.
				for($x = 0; $x < sizeof($participatingUserNames); $x++) {
					$tempClubIds = $this->xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$participatingUserNames[$x].'"]/../@clubId');
					foreach($tempClubIds as $tc) {
						/*if($tc == NULL)
							tempClubIds[$tc]*/
						array_push($userClubIds, trim($tc->nodeValue));
					}
				}
					//Getting users distances for 2015.
				for($x = 0; $x < sizeof($participatingUserNames); $x++) {
					$distance = $this->xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier[@userName = "'.$participatingUserNames[$x].'"]/Log/Entry/Distance');
                    for($y=0; $y<$distance->length; $y++){
                        $sum+= ($distance[$y]->nodeValue);
                    }
                    array_push($participatingDistance, $sum);
                    $sum = 0;
				}
				

					//DB push 2015!
                for($i = 0; $i<sizeOf($participatingUserNames); $i++){
                    try{
						$input=$this->db->prepare("INSERT INTO scs(clubID, skierUName, seasonYear, totalDistance) VALUES (:clubID, :skierUName, :seasonYear, :totalDistance)");
						$input->bindValue(':clubID', 	 	$userClubIds[$i], 				PDO::PARAM_STR);
						$input->bindValue(':skierUName', 	$participatingUserNames[$i],	PDO::PARAM_STR);
						$input->bindValue(':seasonYear', 	'2015',							PDO::PARAM_STR);
						$input->bindValue(':totalDistance', $participatingDistance[$i],		PDO::PARAM_STR);
						$input->execute();
					}
					catch(Exception $e) {
						echo "Something went wrong - SCS2015 - ";
						echo $e->getMessage();
					}
					
					echo $participatingUserNames[$i]. '<br>'.
						 $userClubIds[$i].'<br>'.
						 $participatingDistance[$i].'<br>'.
						 '2015'.'<br>'.'<br>';  
				}
				
                unset($participatingUserNames);
				$participatingUserNames = array();
				unset($userClubIds);
				$userClubIds = array();
				unset($participatingDistance);
				$participatingDistance = array();
				unset($temp);
				$temp = array();
					//Getting usernames for season 2016.
				$temp = $this->xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier/@userName');
                foreach($temp as $t){
                    array_push($participatingUserNames, trim($t->nodeValue));
                }
					//Getting users clubs for 2016.
				for($x = 0; $x < sizeof($participatingUserNames); $x++) {
					$tempClubIds = $this->xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$participatingUserNames[$x].'"]/../@clubId');
					foreach($tempClubIds as $tc) {
						/*if($tc == NULL)
							tempClubIds[$tc]*/
						array_push($userClubIds, trim($tc->nodeValue));
					}
				}
					//Getting users distances for 2016.
				for($x = 0; $x < sizeof($participatingUserNames); $x++) {
					$distance = $this->xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier[@userName = "'.$participatingUserNames[$x].'"]/Log/Entry/Distance');
                    for($y=0; $y<$distance->length; $y++){
                        $sum+= ($distance[$y]->nodeValue);
                    }
                    array_push($participatingDistance, $sum);
                    $sum = 0;
				}
				

					//DB push 2016!
                for($i = 0; $i<sizeOf($participatingUserNames); $i++){
                    try{
						$input=$this->db->prepare("INSERT INTO scs(clubID, skierUName, seasonYear, totalDistance) VALUES (:clubID, :skierUName, :seasonYear, :totalDistance)");
						$input->bindValue(':clubID', 	 	$userClubIds[$i], 				PDO::PARAM_STR);
						$input->bindValue(':skierUName', 	$participatingUserNames[$i],	PDO::PARAM_STR);
						$input->bindValue(':seasonYear', 	'2016',							PDO::PARAM_STR);
						$input->bindValue(':totalDistance', $participatingDistance[$i],		PDO::PARAM_STR);
						$input->execute();
					}
					catch(Exception $e) {
						echo "Something went wrong - SCS2016 - ";
						echo $e->getMessage();
					}
					echo $participatingUserNames[$i]. '<br>'.
						 $userClubIds[$i].'<br>'.
						 $participatingDistance[$i].'<br>'.
						 '2016'.'<br>'.'<br>';
                }
				
		}
		
			
			
			
			
		public function master() {
			$this->getSeason();
			$this->getClubs();
			$this->getSkiers();
			$this->getSCS();
			
			
			//echo "test";
		}
	}
	
?>